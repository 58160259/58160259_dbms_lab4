create view SalePerOrder As
	Select 
		orderNumber, SUM(quantityOrdered * priceEach) total
	FROM
		orderdetails
	GROUP BY orderNumber
	ORDER BY total DESC;
