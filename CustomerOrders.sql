Create view CustomerOrders As
	Select
		d.orderNumber,
		c.customerName,
		SUM(quantityOrdered * priceEach)  total
	FROM
		orderdetails d
			INNER JOIN
		orders o ON o.orderNumber = d.orderNumber
			INNER JOIN
		customers c ON c.customerNumber = o.customerNumber
	GROUP by d.orderNumber
	ORDER by total DESC;
