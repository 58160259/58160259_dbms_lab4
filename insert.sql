INSERT INTO major values('1','คอมพิวเตอร์ธุรกิจ','Computer Business');
INSERT INTO major values('2','วิทยาการคอมพิวเตอร์','Computer Science');
INSERT INTO major values('3','เศรษฐศาสตร์','Economic');

INSERT INTO title values('1','นาย','MR.');
INSERT INTO title values('2','นางสาว','Miss');
INSERT INTO title values('3','นาง','Mrs.');

INSERT INTO student values('57161111','ดุ้กกิ้ก','ใจดี','Dukdik','Jaidee','2','1');
INSERT INTO student values('57162222','ต้อกแต็ก','ใจเย็น','Toktak','jaiyen','2','2');
INSERT INTO student values('57163333','บูรพา','สุขใจ','Burapha','Sukjai','1','3');

