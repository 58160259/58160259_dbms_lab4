DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS title;
DROP TABLE IF EXISTS major;

Create TABLE title
(
	tid	VARCHAR(5),
	tname 	VARCHAR(30),
	ename	VARCHAR(30),
	PRIMARY KEY(tid)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE major
(
	id	varchar(5),
	tname	varchar(30),
	ename	varchar(30),
	PRIMARY KEY(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE student
(
	id     	  varchar(10),
	tname  	  varchar(30),
	tsurname  varchar(50),
	ename	  varchar(30),
	esurname  varchar(50),
	title_id  varchar(5),
	major_id  varchar(5),
	PRIMARY KEY (id),
	FOREIGN KEY (title_id) REFERENCES title(tid),
	FOREIGN KEY (major_id) REFERENCES major(id)
) DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
	
